package com.odiotte.test.process;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.odiotte.api.processmanager.process.IProcess;

public class TestProcess implements IProcess{

	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@SuppressWarnings("null")
	public void execute() throws InterruptedException{
		
		logger.info("PROCESS TEST");
		
		String test = null;
		
		logger.info("sleep 10 sec");
		Thread.sleep(10000);
		logger.info("woke up");
		
		//test.concat("Test when process throws RunTimeException");
	}
}
