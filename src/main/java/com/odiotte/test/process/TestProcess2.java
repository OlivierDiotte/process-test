package com.odiotte.test.process;

import java.sql.Timestamp;
import java.util.Calendar;

import javax.mail.Folder;
import javax.mail.MessagingException;
import javax.mail.internet.InternetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.mobilemaestria.commons.email.adapter.MobileMaestriaIMAPAdapter;
import com.mobilemaestria.commons.email.adapter.MobileMaestriaSMTPAdapter;
import com.mobilemaestria.commons.email.helper.MobileMaestriaEmailHelper;
import com.mobilemaestria.commons.email.util.AddressUtil;
import com.odiotte.api.processmanager.process.IProcess;
import com.odiotte.api.processmanager.util.ProcessUtil;
import com.odiotte.email.dto.EmailDTO;
import com.odiotte.email.manager.IMAPManager;
import com.odiotte.email.manager.SMTPManager;

public class TestProcess2 implements IProcess {

	final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Override
	public void execute() throws InterruptedException{
		logger.info("START ---- TEST PROCESS 2");
		
		//testCalendar();
		//sendEmail();
		//readEmail();
		
		
		
		
		
		logger.info("END ---- TEST PROCESS 2");
	}
	private void testCalendar(){
		Timestamp currentTime = ProcessUtil.getCurrentTimestamp();
		
		Calendar calCurrent = Calendar.getInstance();		
		calCurrent.setTime(currentTime);
		
		Calendar calAjusted = Calendar.getInstance();		
		calAjusted.setTime(currentTime);
		calAjusted.set(Calendar.SECOND, 0);
		
		System.out.println("--ajusted:"+calAjusted.getTimeInMillis());
		System.out.println("--current:"+calCurrent.getTimeInMillis());
		System.out.println("--calAjusted.before(calCurrent):"+calAjusted.before(calCurrent));
		
		/*if(processEntity.getEachDayHour()!=null){
			calAjusted.set(Calendar.HOUR_OF_DAY, processEntity.getEachDayHour());
		}
		if(processEntity.getEachDayMinute()!=null){
			calAjusted.set(Calendar.MINUTE, processEntity.getEachDayMinute());
		}
		*/
		if(calAjusted.before(calCurrent)){
			calAjusted.add(Calendar.HOUR_OF_DAY, 24);
		}
		
		System.out.println(calAjusted.getTimeInMillis() - calCurrent.getTimeInMillis());
	}
	private void sendEmail(){
		
		String emailTo = "odiotte@mobilemaestria.com";
		
		logger.info("||| SENDING EMAIL TO:"+emailTo);
		EmailDTO email = new EmailDTO();
		 
		email.setEmailTo(new InternetAddress[] { AddressUtil.parseAddress(emailTo) });
		email.setEmailFrom(MobileMaestriaEmailHelper.getTelecomAccountEmailAddress("TEST"));
		email.setEmailSubject("TEST EMAIL");
		
		email.setEmailBody(buildEmailBody());
		
		SMTPManager manager = new SMTPManager(new MobileMaestriaSMTPAdapter());
		manager.sendEmail(email);
	}
	private String buildEmailBody(){
		StringBuffer body = new StringBuffer();
		
		body.append("Toutes les donn�es des fournisseurs de service, pour ce client, ont �t� charg�es pour le mois indiqu�. C'est complet. Vous pouvez � pr�sent envoyer les �tats de compte.");
		body.append("<br/>");
		body.append("<br/>");
		body.append("------------------------------------------------------------------------------------------------------------------------------------------------------------------------");
		body.append("<br/>");
		body.append("<br/>");
		body.append("Billing information for this customer has been downloaded for the month indicated. Everything is complete. You can now send your statements of account.");
		body.append("<br/>");
		
		logger.info("||| "+body.toString());
		
		return body.toString();
	}
	/*private void readEmail(){
		System.out.println("START readEmail()");
		IMAPManager manager = new IMAPManager(new MobileMaestriaIMAPAdapter());
		Folder folder = manager.getFolder("Inbox");
		
		try {
			System.out.println("MessageCount:"+folder.getMessageCount());
		} catch (MessagingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		//System.gc();
		System.out.println("END readEmail()");
	}*/
}
